package com.example.springchinook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringChinookApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringChinookApplication.class, args);
    }

}
