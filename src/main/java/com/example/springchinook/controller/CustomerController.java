package com.example.springchinook.controller;

import com.example.springchinook.data.CustomerRepository;
import com.example.springchinook.dto.Wrapper;
import com.example.springchinook.models.Country;
import com.example.springchinook.models.Customer;
import com.example.springchinook.models.Spender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerController(CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }

    @GetMapping("/allCustomers")
    public List<Customer> getAllCustomers(){
        return customerRepository.getCustomers();
    }
    @GetMapping("/customer")
    public Customer getCustomer(@RequestParam int id){
        return customerRepository.getCustomer(id);
    }

    @GetMapping("/customer/byName")
    public List<Customer> searchByName(@RequestParam String input){
        return customerRepository.customerByName(input);
    }

    @GetMapping("/customer/page")
    public List<Customer> offsetAndLimitCustomers(@RequestParam List<Integer> input){
        return customerRepository.customerPage(input.get(0), input.get(1));
    }

    @GetMapping("/customer/countries")
    public List<Country> getCountries(){
        return customerRepository.customerByCountry();
    }

    @GetMapping("/customer/spenders")
    public List<Spender> getSpenders(){
        return customerRepository.getSpenders();
    }


    @GetMapping("/customer/genres/{id}")
    public Wrapper getGenres(@PathVariable int id){
        return customerRepository.getPopularGenre(id);
    }

    @PostMapping("/customer")
    public ResponseEntity<Customer> addCustomer(@RequestBody Customer customer){
        if(customerRepository.checkExist(customer.getCustomerId())){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        if(!customerRepository.validCustomer(customer)){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        customerRepository.addCustomer(customer);
        return new ResponseEntity<Customer>(customerRepository.getCustomer(customer.getCustomerId()), HttpStatus.OK);
    }

    @PutMapping("/customer")
    public ResponseEntity<Customer> modifyCustomer(@RequestBody Customer customer){
        if(!customerRepository.checkExist(customer.getCustomerId())){
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        if(!customerRepository.validCustomer(customer)){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        customerRepository.modifyCustomer(customer);
        return new ResponseEntity<Customer>(customerRepository.getCustomer(customer.getCustomerId()), HttpStatus.OK);
    }


}
