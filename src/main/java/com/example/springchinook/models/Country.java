package com.example.springchinook.models;

public class Country {
    private String country;
    private int num_Customers;

    public Country(String country, int num_Customers) {
        this.country = country;
        this.num_Customers = num_Customers;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getNum_Customers() {
        return num_Customers;
    }

    public void setNum_Customers(int num_Customers) {
        this.num_Customers = num_Customers;
    }
}
