package com.example.springchinook.models;

public class Spender {
    private int customerId;
    private String firstName;
    private String lastName;
    private int invoiceTotal;

    public Spender(int customerId, String firstName, String lastName, int invoiceTotal) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.invoiceTotal = invoiceTotal;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getInvoiceTotal() {
        return invoiceTotal;
    }

    public void setInvoiceTotal(int invoiceTotal) {
        this.invoiceTotal = invoiceTotal;
    }
}
