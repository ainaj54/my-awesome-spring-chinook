package com.example.springchinook.dto;

import org.springframework.http.HttpStatus;

public class Wrapper {
    private  Object data;
    private String customerName;
    private HttpStatus status;

    public Wrapper(Object data, String customerName, HttpStatus status) {
        this.data = data;
        this.customerName = customerName;
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}
