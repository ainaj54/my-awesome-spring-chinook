package com.example.springchinook.dto;

public class CustomerPopularGenreDto {

    private String genre;
    private int genreTotal;

    public CustomerPopularGenreDto(String genre, int genreTotal) {

        this.genre = genre;
        this.genreTotal = genreTotal;
    }


    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getGenreTotal() {
        return genreTotal;
    }

    public void setGenreTotal(int genreTotal) {
        this.genreTotal = genreTotal;
    }
}
