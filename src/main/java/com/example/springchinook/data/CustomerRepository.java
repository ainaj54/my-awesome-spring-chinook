package com.example.springchinook.data;


import com.example.springchinook.dto.CustomerPopularGenreDto;
import com.example.springchinook.dto.Wrapper;
import com.example.springchinook.models.Country;
import com.example.springchinook.models.Customer;
import com.example.springchinook.models.Spender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerRepository {
    @Value("${spring.datasource.url}")
    private String url;

    public List<Customer> getCustomers(){
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer";
        List<Customer> customers = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url)){
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while(resultSet.next()){
                    customers.add(new Customer(resultSet.getInt("customer_id"), resultSet.getString("first_name"),resultSet.getString( "last_name"),resultSet.getString( "country"), resultSet.getString( "postal_code"), resultSet.getString("phone"), resultSet.getString("email")));
                }
            }
        }catch (SQLException e){
            e.printStackTrace();
        }


    return customers;
    }


    public Customer getCustomer(int id) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer WHERE customer_id = ?";
        Customer customer = null;
        try (Connection connection = DriverManager.getConnection(url)) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    customer = new Customer(resultSet.getInt("customer_id"), resultSet.getString("first_name"), resultSet.getString("last_name"), resultSet.getString("country"), resultSet.getString("postal_code"), resultSet.getString("phone"), resultSet.getString("email"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    public  List<Customer> customerByName(String input){
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer WHERE concat(first_name, last_name) LIKE ?";
        List<Customer> customers = new ArrayList();

        try(Connection connection = DriverManager.getConnection(url)){
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,'%' + input + '%');
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while(resultSet.next()){
                    customers.add(new Customer(resultSet.getInt("customer_id"), resultSet.getString("first_name"), resultSet.getString("last_name"), resultSet.getString("country"), resultSet.getString("postal_code"), resultSet.getString("phone"), resultSet.getString("email")));
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }

        return customers;
    }

    public List<Customer> customerPage(int limit, int offset){
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer LIMIT ? OFFSET ?";
        List<Customer> customer = new ArrayList<>();

        try(Connection connection = DriverManager.getConnection(url)){
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2,offset);
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while(resultSet.next()){
                    customer.add(new Customer(resultSet.getInt("customer_id"), resultSet.getString("first_name"), resultSet.getString("last_name"), resultSet.getString("country"), resultSet.getString("postal_code"), resultSet.getString("phone"), resultSet.getString("email")));
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return customer;
    }

    public List<Country> customerByCountry(){
        String sql = "SELECT country, COUNT(customer_id) AS num_customers FROM customer GROUP BY country ORDER BY num_customers DESC";
        List<Country> countries = new ArrayList<>();

        try(Connection connection = DriverManager.getConnection(url)){
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while(resultSet.next()){
                    countries.add(new Country(resultSet.getString("country"), resultSet.getInt("num_customers")));
                }
            }

        }catch(SQLException e){
            e.printStackTrace();
        }
        return countries;
    }


    public List<Spender> getSpenders(){
        String sql = "SELECT customer.customer_id AS id, customer.first_name AS firstName, customer.last_name AS lastName, customer.country, SUM(invoice.total) AS sum_invoice FROM customer INNER JOIN invoice ON customer.customer_id = invoice.customer_id GROUP BY id ORDER BY sum_invoice DESC";
        List<Spender> spenders = new ArrayList<>();

        try(Connection connection = DriverManager.getConnection(url)){
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            try(ResultSet resultSet = preparedStatement.executeQuery()){
                while(resultSet.next()){
                    spenders.add(new Spender(resultSet.getInt("id"), resultSet.getString("firstName"), resultSet.getString("lastName"), resultSet.getInt("sum_invoice")));
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return spenders;
    }

    public Wrapper getPopularGenre(int id){

        Customer customer = getCustomer(id);
        String customerName = customer.getFirstName() + ' ' + customer.getLastName();


        String sql = "SELECT g.\"name\" as genre_name, COUNT (il.invoice_line_id) as genre_total\n" +
                "FROM genre as g\n" +
                "INNER JOIN track as t ON t.genre_id = g.genre_id\n" +
                "INNER JOIN invoice_line as il ON il.track_id = t.track_id\n" +
                "INNER JOIN invoice as inv ON inv.invoice_id = il.invoice_id\n" +
                "INNER JOIN customer as c ON c.customer_id = inv.customer_id AND c.customer_id = ?\n" +
                "GROUP BY g.genre_id, g.\"name\"\n" +
                "ORDER BY genre_total DESC\n" +
                "FETCH FIRST 1 ROWS with TIES";
                List<CustomerPopularGenreDto> genres = new ArrayList<>();

                try(Connection connection = DriverManager.getConnection(url)){
                    PreparedStatement preparedStatement = connection.prepareStatement(sql);
                    preparedStatement.setInt(1, id);
                    try(ResultSet resultSet = preparedStatement.executeQuery()){
                        while(resultSet.next()){
                            genres.add(new CustomerPopularGenreDto(resultSet.getString("genre_name"), resultSet.getInt("genre_total")));
                        }
                    }
                }catch(SQLException e){
                    e.printStackTrace();
                }

        return new Wrapper( genres, customerName, HttpStatus.OK);
    }

    public Customer addCustomer(Customer customer){
        String sql = "INSERT INTO customer(customer_id, first_name, last_name, country, postal_code, phone, email)\n" +
                "VALUES(?, ?, ?, ?, ?, ?, ?)";

        try(Connection connection = DriverManager.getConnection(url)){
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4,customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhoneNumber());
            preparedStatement.setString(7, customer.getEmail());
            preparedStatement.executeUpdate();

        }catch(SQLException e){
            e.printStackTrace();
        }


        return getCustomer(customer.getCustomerId());
    }

    public boolean checkExist(int customerId) {
        if(getCustomer(customerId) == null){
            return false;
        }

        return true;
    }

    public boolean validCustomer(Customer customer) {
        return customer.getCustomerId() != 0 && customer.getFirstName() != null && customer.getLastName() != null && customer.getCountry() != null
                && customer.getPhoneNumber() != null && customer.getPostalCode() != null && customer.getEmail()!= null;
    }

    public Customer modifyCustomer(Customer customer) {
        boolean success = false;

        String sql = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer_id = ?";
        try(Connection connection = DriverManager.getConnection(url)){
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3,customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhoneNumber());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setInt(7, 8);
            preparedStatement.executeUpdate();



        }catch (SQLException e){
            e.printStackTrace();
        }
        return getCustomer(customer.getCustomerId());
    }
}
